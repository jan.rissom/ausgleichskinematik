#include <PID_v1.h>

#include <Wire.h>
#include <math.h>



float DXL_PROTOCOL_VERSION = 2.0;


double l = 413000;
double angle = 45;
double error = 0;
String data;



 ////PI////
double Kp = 0.7;          // (P)roportional Tuning Parameter
double Ki = 1.5;          // (I)ntegral Tuning Parameter     
double Kd = 0.005 ;
float maxPID = 100;    // The maximum value that can be output
double pi_position = 0;
double setpoint = 0;

#define LOOP_TIME 70          
unsigned long timerValue = 0;

//PID controller(&error, &pi_position, &setpoint, Kp, Ki, Kd, REVERSE); 

void setup() 
{
  Serial1.begin(115200);
  pinMode(10, OUTPUT);
  pinMode(11,OUTPUT);
  timerValue = millis();
  digitalWrite(10, LOW);
  digitalWrite(11, HIGH);
  delay(1000);
  digitalWrite(11, LOW);
  digitalWrite(10, HIGH);
  digitalWrite(11, HIGH);
  delay(500);
  digitalWrite(11, LOW);
//  controller.SetSampleTime(LOOP_TIME);
//  controller.SetOutputLimits(-maxPID, maxPID);
//  controller.SetMode(AUTOMATIC);
}

void loop() 
{ 
  if (Serial1.available() > 0)
  { 
    data = Serial1.readStringUntil('\n');
    error = data.toDouble();
    if (error < 0)
    {
      digitalWrite(10, HIGH);
    }
    else
    {
      digitalWrite(10, LOW);
    }
    if (error < 50 && error > -50)
    {
      digitalWrite(11, LOW);
    }
    else
    {
      digitalWrite(11, HIGH);
    }  
    Serial1.flush();
    Serial1.println(error); 
    Serial1.println(pi_position);
  }
}
