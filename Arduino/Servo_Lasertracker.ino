#include <PID_v1.h>

#include <Wire.h>
#include <math.h>
#include <DynamixelShield.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

byte mac[] = {
  0xA8, 0x61, 0x0A, 0xAE, 0x30, 0x0A
};
IPAddress ip(192, 168, 0, 177);

unsigned int localPort = 10000;


EthernetUDP Udp;

const uint8_t DXL_ID = 1;
const float DXL_PROTOCOL_VERSION = 2.0;

DynamixelShield dxl;

int l = 413;
double angle = 45;
float error = 0;
char zahl[27];
char packetBuffer[128];  // buffer to hold incoming packet,

long start_time;
long cur_time;
float track_time;
double show;

using namespace ControlTableItem;


void setup() 
{
  // For Uno, Nano, Mini, and Mega, use UART port of DYNAMIXEL Shield to debug.
  Serial1.begin(115200);

  //Set Port baudrate to 57600bps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(57600);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
  // Get DYNAMIXEL information
  dxl.ping(DXL_ID);

  // Turn off torque when configuring items in EEPROM area
  dxl.torqueOff(DXL_ID);
  dxl.setOperatingMode(DXL_ID, OP_POSITION);
  dxl.writeControlTableItem(POSITION_P_GAIN, DXL_ID, 7000);
  dxl.writeControlTableItem(POSITION_I_GAIN, DXL_ID, 2);
  dxl.writeControlTableItem(POSITION_D_GAIN, DXL_ID, 0);
  dxl.writeControlTableItem(VELOCITY_P_GAIN, DXL_ID, 4500);
  dxl.writeControlTableItem(VELOCITY_I_GAIN, DXL_ID, 2);
  dxl.writeControlTableItem(FEEDFORWARD_1ST_GAIN, DXL_ID, 0);
  dxl.writeControlTableItem(FEEDFORWARD_2ND_GAIN, DXL_ID, 0);
  dxl.torqueOn(DXL_ID);
  dxl.setGoalPosition(DXL_ID, 90, UNIT_DEGREE);
  delay(2000); 
  dxl.setGoalPosition(DXL_ID, 45, UNIT_DEGREE);
  delay(5000);

  Ethernet.begin(mac, ip);

  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial1.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial1.println("Ethernet cable is not connected.");
  }

  // start UDP
  Udp.begin(localPort);
  pinMode(12,OUTPUT);
  digitalWrite(12,HIGH);
  delay(5000);
  start_time = millis();
}


void loop() 
{ 
 
  digitalWrite(12,LOW);
  Udp.parsePacket();
  Udp.read(packetBuffer, 128);
  ParsePacket(); 
  error = atof(zahl);
  angle = 45-((atan(error/l)/2)*(180/M_PI));
  dxl.setGoalPosition(DXL_ID, angle, UNIT_DEGREE);  
  cur_time = millis()-start_time;
  Serial1.print(cur_time);
  Serial1.print(",");
  Serial1.print(packetBuffer); 
  Serial1.print(","); 
  Serial1.print(error);
  Serial1.print(","); 
  Serial1.println(angle);
}

void ParsePacket()
{
  for (int i=2; i <27;i++)
  {
    zahl[i-2] = packetBuffer[i];
  }
  zahl[26] = '\0'; 
}
