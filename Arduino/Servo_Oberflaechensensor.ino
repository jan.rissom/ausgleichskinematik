#include <Wire.h>
#include <math.h>
#include <Encoder.h>
#include <DynamixelShield.h>


Encoder sick(20, 21);

#if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MEGA2560)
  #include <SoftwareSerial.h>
  SoftwareSerial soft_serial(7, 8); // DYNAMIXELShield UART RX/TX
  #define DEBUG_SERIAL soft_serial
#elif defined(ARDUINO_SAM_DUE) || defined(ARDUINO_SAM_ZERO)
  #define DEBUG_SERIAL SerialUSB    
#else
  #define DEBUG_SERIAL Serial
#endif

const uint8_t DXL_ID = 1;
const float DXL_PROTOCOL_VERSION = 2.0;

DynamixelShield dxl;

double l = 4130;
double angle = 45;
int last = 0;

using namespace ControlTableItem;

void setup() 
{
  // For Uno, Nano, Mini, and Mega, use UART port of DYNAMIXEL Shield to debug.
  Serial1.begin(115200);

  //Set Port baudrate to 57600bps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(57600);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
  // Get DYNAMIXEL information
  dxl.ping(DXL_ID);

  // Turn off torque when configuring items in EEPROM area
  dxl.torqueOff(DXL_ID);
  dxl.setOperatingMode(DXL_ID, OP_POSITION);
//  DEBUG_SERIAL.println(dxl.readControlTableItem(POSITION_P_GAIN, DXL_ID));
//  DEBUG_SERIAL.println(dxl.readControlTableItem(POSITION_I_GAIN, DXL_ID));
//  DEBUG_SERIAL.println(dxl.readControlTableItem(POSITION_D_GAIN, DXL_ID));
  
  dxl.writeControlTableItem(POSITION_P_GAIN, DXL_ID, 7000);
  dxl.writeControlTableItem(POSITION_I_GAIN, DXL_ID, 2);
  dxl.writeControlTableItem(POSITION_D_GAIN, DXL_ID, 0);
  dxl.writeControlTableItem(VELOCITY_P_GAIN, DXL_ID, 4500);
  dxl.writeControlTableItem(VELOCITY_I_GAIN, DXL_ID, 2);
  dxl.writeControlTableItem(FEEDFORWARD_1ST_GAIN, DXL_ID, 0);
  dxl.writeControlTableItem(FEEDFORWARD_2ND_GAIN, DXL_ID, 0);
  dxl.torqueOn(DXL_ID);
  dxl.setGoalPosition(DXL_ID, 90, UNIT_DEGREE);
  delay(2000); 
  dxl.setGoalPosition(DXL_ID, 45, UNIT_DEGREE);
  delay(5000);


  
}

void loop() 
{  
    if (last != sick.read())
    {
      Serial1.println(sick.read());
      angle = calc_err(sick.read()); 
      //DEBUG_SERIAL.println(angle); 
      dxl.setGoalPosition(DXL_ID, angle, UNIT_DEGREE);
      last = sick.read();
    } 
  //} 
}

double calc_err(double data)
{
  return 45-((atan(data/l)/2)*(180/M_PI));
}
