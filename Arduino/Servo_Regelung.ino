#include <PID_v1.h>

#include <Wire.h>
#include <math.h>
#include <DynamixelShield.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 0, 177);

unsigned int localPort = 10000;

const uint8_t DXL_ID = 1;
const float DXL_PROTOCOL_VERSION = 2.0;

DynamixelShield dxl;

double l = 413000;
double angle = 45;
double error = 0;
String data;
using namespace ControlTableItem;



 ////PI////
double Kp = 0.7;          // (P)roportional Tuning Parameter
double Ki = 1.5;          // (I)ntegral Tuning Parameter     
double Kd = 0.005 ;
float maxPID = 100000;    // The maximum value that can be output
double pi_position = 0;
double setpoint = 0;

#define LOOP_TIME 70          
unsigned long timerValue = 0;

PID controller(&error, &pi_position, &setpoint, Kp, Ki, Kd, REVERSE); 

void setup() 
{
  // For Uno, Nano, Mini, and Mega, use UART port of DYNAMIXEL Shield to debug.
  Serial1.begin(115200);

  //Set Port baudrate to 57600bps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(57600);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
  // Get DYNAMIXEL information
  dxl.ping(DXL_ID);

  // Turn off torque when configuring items in EEPROM area
  dxl.torqueOff(DXL_ID);
  dxl.setOperatingMode(DXL_ID, OP_POSITION);
  dxl.writeControlTableItem(POSITION_P_GAIN, DXL_ID, 7000);
  dxl.writeControlTableItem(POSITION_I_GAIN, DXL_ID, 2);
  dxl.writeControlTableItem(POSITION_D_GAIN, DXL_ID, 0);
  dxl.writeControlTableItem(VELOCITY_P_GAIN, DXL_ID, 4500);
  dxl.writeControlTableItem(VELOCITY_I_GAIN, DXL_ID, 2);
  dxl.writeControlTableItem(FEEDFORWARD_1ST_GAIN, DXL_ID, 0);
  dxl.writeControlTableItem(FEEDFORWARD_2ND_GAIN, DXL_ID, 0);
  dxl.torqueOn(DXL_ID);
  dxl.setGoalPosition(DXL_ID, 90, UNIT_DEGREE);
  delay(2000); 
  dxl.setGoalPosition(DXL_ID, 45, UNIT_DEGREE);
  delay(5000);
  timerValue = millis();

  controller.SetSampleTime(LOOP_TIME);
  controller.SetOutputLimits(-maxPID, maxPID);
  controller.SetMode(AUTOMATIC);
}

void loop() 
{ 
  if (Serial1.available() > 0)
  { 
    data = Serial1.readStringUntil('\n');
    error = data.toDouble();
    controller.Compute();
    angle = 45+((atan(pi_position/l)/2)*(180/M_PI));
    dxl.setGoalPosition(DXL_ID, angle, UNIT_DEGREE);
    Serial1.flush();
    Serial1.println(error); 
    Serial1.println(pi_position);
  }
}
