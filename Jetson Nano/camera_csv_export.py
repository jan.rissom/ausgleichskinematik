
import threading
import sys
import cv2
import numpy 
import csv
import os
from typing import Optional
from vimba import *

#Variables
centerpoint = 723
ratio = 0.17 #0.126


#Process start message
def print_preamble():
    print('Starting controller...\n')


#Abort message
def abort(reason: str, return_code: int = 1, usage: bool = False):
    print(reason + '\n')
    sys.exit(return_code)

#Error message camera
def parse_args() -> Optional[str]:
    args = sys.argv[1:]
    argc = len(args)

    for arg in args:
        if arg in ('/h', '-h'):
            sys.exit(0)

    if argc > 1:
        abort(reason="Invalid number of arguments. Abort.", return_code=2, usage=True)

    return None if argc == 0 else args[0]

#Connect available cameras
def get_camera(camera_id: Optional[str]) -> Camera:
    with Vimba.get_instance() as vimba:
        if camera_id:
            try:
                return vimba.get_camera_by_id(camera_id)

            except VimbaCameraError:
                abort('Failed to access Camera \'{}\'. Abort.'.format(camera_id))

        else:
            cams = vimba.get_all_cameras()
            if not cams:
                abort('No Cameras accessible. Abort.')

            return cams[0]

#Camera setup 
def setup_camera(cam: Camera):
    with cam:
        #Disable auto exposure
        try:
            cam.ExposureAuto.set('Off')

        except (AttributeError, VimbaFeatureError):
            pass

        #Set exposure mode to a fixed time
        try:
            cam.ExposureMode.set('Timed')

        except (AttributeError, VimbaFeatureError):
            pass
        #Set exposure time
        try:
            cam.ExposureTimeAbs.set('4506')

        except (AttributeError, VimbaFeatureError):
            pass
        #Disable trigger
        try:
            cam.TriggerMode.set('Off')

        except (AttributeError, VimbaFeatureError):
            pass
        #Set Height
        try:
            cam.Height.set('972')
            
        except (AttributeError, VimbaFeatureError):
            pass
        #Set Width
        try:
             cam.Width.set('1296')
            
        except (AttributeError, VimbaFeatureError):
            pass
        #Set OffsetX
        try:
             cam.OffsetX.set('648')
            
        except (AttributeError, VimbaFeatureError):
            pass
        #Set OffsetY
        try:
             cam.OffsetY.set('486')
            
        except (AttributeError, VimbaFeatureError):
            pass
        #Set framerate max
        try:
            cam. AcquisitionFrameRateAbs.set('25')

        except (AttributeError, VimbaFeatureError):
            pass 
            #Disable whitebalance
        try:
            cam.BalanceWhiteAuto.set('Off')

        except (AttributeError, VimbaFeatureError):
            pass

        # Try to adjust GeV packet size. This Feature is only available for GigE - Cameras.
        try:
            cam.GVSPAdjustPacketSize.run()

            while not cam.GVSPAdjustPacketSize.is_done():
                pass

        except (AttributeError, VimbaFeatureError):
            pass

        # Query available, open_cv compatible pixel formats
        # prefer color formats over monochrome formats
        cv_fmts = intersect_pixel_formats(cam.get_pixel_formats(), OPENCV_PIXEL_FORMATS)
        color_fmts = intersect_pixel_formats(cv_fmts, COLOR_PIXEL_FORMATS)

        if color_fmts:
            cam.set_pixel_format(color_fmts[0])

        else:
            mono_fmts = intersect_pixel_formats(cv_fmts, MONO_PIXEL_FORMATS)

            if mono_fmts:
                cam.set_pixel_format(mono_fmts[0])

            else:
                abort('Camera does not support a OpenCV compatible format natively. Abort.')

#Frame handler function
class Handler():
    def __init__(self):
        
        self.shutdown_event = threading.Event()

    def __call__(self, cam: Camera, frame: Frame):
        ENTER_KEY_CODE = 13       
        

        global centerpoint


        key = cv2.waitKey(1)
        if key == ENTER_KEY_CODE:
            self.shutdown_event.set()
            return
        #Acquire new frame
        elif frame.get_status() == FrameStatus.Complete:
           
            # Print current frame ID
            print('{}'.format(frame.get_id()), flush=True)
            
            #msg = 'Stream from \'{}\'. Press <Enter> to stop stream.'
            #cv2.imshow(msg.format(cam.get_name()), frame.as_opencv_image())
            #Process frame
           
            img_raw= frame.as_numpy_ndarray()                        #Get frame as numpy array
            img_reshaped = img_raw.reshape(img_raw.shape[0], -1)     #Reshape array for 2D processing
            img_reshaped[img_reshaped <= 240] = 0                    #Threshold image
            spot = numpy.argwhere(img_reshaped >= 240)               #Get spot coordinates
            point_spot = numpy.median(spot, axis=0)                  #Get center point of spot                    
            spotx = int(point_spot[0])
            spoty = int(point_spot[1])

            #Export Frame
            #path = '/home/jetson/Desktop/Vimba_5_0/VimbaPython/Examples/Frames'
            #cv2.imwrite(os.path.join(path ,''+str(frame.get_id()))+'.jpg', frame.as_opencv_image())
    

            #Export to csv
            with open('Spot_export.csv', 'a', newline='') as csvfile:
                csvwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                csvwriter.writerow('{}'.format(frame.get_id()) + ',{}'.format(frame.get_timestamp()) + ',{}'.format(spotx) + ',{}'.format(spoty))
        cam.queue_frame(frame)



def main():


    #Open csv file for data logging
    with open('Spot_export.csv', 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow('FrameID,Timestamp,PosX,PosY')
    
    print_preamble()
    cam_id = parse_args()

    with Vimba.get_instance():
        with get_camera(cam_id) as cam:

            # Start Streaming, wait for five seconds, stop streaming
            setup_camera(cam)
            handler = Handler()

            try:
                cam.start_streaming(handler=handler, buffer_count=100)

                handler.shutdown_event.wait()

            finally:
                cam.stop_streaming()
             


if __name__ == '__main__':
    main()
